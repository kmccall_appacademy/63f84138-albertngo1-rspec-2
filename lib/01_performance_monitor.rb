def measure(n = 1, &prc)

  time_start = Time.now

  n.times {prc.call}

  (Time.now - time_start) / n

end
