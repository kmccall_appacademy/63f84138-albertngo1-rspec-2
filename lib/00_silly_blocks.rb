def reverser(&prc)



  prc.call.split(" ").map {|word| word.reverse}.join(" ")


end

def adder(add = 1, &prc)

  prc.call + add

end

def repeater(repeat = 1, &prc)

  repeat.times { prc.call }

end
